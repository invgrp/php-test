<?php
/**
 * Created by PhpStorm.
 * User: vgdomski
 * Date: 25.04.16
 * Time: 21:37
 */

namespace InnovationGroup\Data;


class DataJson extends Data implements IData
{
    public function load()
    {
        $data = file_get_contents($this->file);
        $data = trim($data);

        $data = json_decode($data, true);

        if(!$data){
            throw new \Exception('Bad json data', 503);
        }
        else {

            $array = [];
            foreach ($data as $item){
                $array[$item[3]][$item[0]] =
                    [
                        'name' => $item[1],
                        'price' => (float) str_replace(',','.',$item[2]), ///but why u use string?
                        'code' => $item[0], ///but why u use string? 
                        'group' => $item[3], ///but why u use string?
                    ]
                ;
            }
            $this->data = $array;
            return $array;
        }
    }
}