<?php

/**
 * Created by PhpStorm.
 * User: vgdomski
 * Date: 25.04.16
 * Time: 21:35
 */
namespace InnovationGroup\Data;

interface IData
{
    /**
     * 
     * Load data from file and convert to assoc array.
     * 
     * @return array|null data
     */
    public function load();

    /**
     * Format data by params from post 
     * @param $params array (after validator)
     * @return mixed
     */
    public function format($params);

    /**
     * @return array data
     */
    public function get_data();
}