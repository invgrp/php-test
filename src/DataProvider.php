<?php
/**
 * Created by PhpStorm.
 * User: vgdomski
 * Date: 27.04.16
 * Time: 6:39
 */

namespace InnovationGroup\Data;


class DataProvider
{
    /**
     * @param $type
     * @return IData
     * @throws \Exception
     */
    public static function get($type)
    {

        $data = null;

        switch ($type) {
            case 'php': {
                $data = DataPhp::class;
                break;
            }
            case 'json': {
                $data = DataJson::class;
                break;
            }
            case 'xml': {
                $data = DataXml::class;
                break;
            }
            default :
                throw new \Exception('No data provider found for ' . $type);
                break;
        }
        
        return $data;
    }
}