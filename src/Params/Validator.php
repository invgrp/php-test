<?php
/**
 * Created by PhpStorm.
 * User: vgdomski
 * Date: 27.04.16
 * Time: 3:53
 */

namespace InnovationGroup\Params;


class Validator
{
    public static $defaults = array(
        'data_source' => array(
            'required'  => true,
            'type' => 'string',
            'values' => ['php', 'json', 'xml'],
        ),
        'group' => array(
            'required'  => false,
            'type' => 'string',
            'values' => ['europe', 'world', 'all'],
        ),
        'filter' => array(
            'required'  => false,
            'type' => 'array'
        ),
        'sort_column' => array(
            'required'  => false,
            'type' => 'string',
            'values' => ['price', 'code', 'name', 'unsorted'],
        ),
        'sort_direction' => array(
            'required'  => false,
            'type' => 'string',
            'values' => ['ascending', 'descending'],
        ),
    );
    private static function options(array $params)
    {
        $return = array();
        foreach($params as $param)
        {
            if(array_key_exists($param,self::$defaults)) {
                $return[$param] = self::$defaults[$param];
            }
            else {
                $return[$param] = array(
                    'required'  => false,
                    'type' => null,
                );
            }
        }
        return $return;
    }
    public static function validate(array $data)
    {
        $needs = self::options([
            'data_source',
            'group',
            'filter',
            'sort_direction',
            'sort_column'
        ]);
        
        $return = [ true ];
        foreach($needs as $need=>$params)
        {
            if(isset($params['required'])&&$params['required']&&!isset($data[$need])) {
                return [false, "Parameter '{$need}' not found." ];
            }
            else {
                $value =  null ;
                if(isset($data[$need])) {
                    switch ($params['type']) {
                        case 'string':{
                            $value = (string) $data[$need];
                            
                            if(isset($params['values'])){
                                if(empty($value)) {
                                    continue;
                                }
                                if(!in_array($value, $params['values'])){
                                    return [false ,"Available values for '{$need}' is [" . implode(', ', $params['values']) . ']'];
                                }
                                $return[$need] = $value;
                            }
                            break;
                        }
                        case 'array': {
                            if(is_array($data[$need])) {
                                $value = (array) $data[$need];
                            }
                            else {
                                return [false, "'{$need}' parameter should be {$params['type']} type"];
                            }
                            $return[$need] = $value;
                            break;
                        }
                        default :
                            $value = $data[$need];
                    }
                }
            }
        }
        return $return;
    }

}