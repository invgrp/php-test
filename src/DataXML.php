<?php
/**
 * Created by PhpStorm.
 * User: vgdomski
 * Date: 25.04.16
 * Time: 21:37
 */

namespace InnovationGroup\Data;


class DataXml extends Data implements IData
{
    public function load()
    {
        $data = file_get_contents($this->file);

        $xml = simplexml_load_string($data);
        $xml = ((array) $xml);
        $xml = array_values($xml)[0];
        $data = [];
        /**
         * @var $item \SimpleXMLElement
         */
        foreach ($xml as $item) {
            $attributes = ((array)$item->attributes());
            $attributes = $attributes['@attributes']['Type'];
            $code = (string)$item->Code;
            $data[$attributes][$code] = [
                    'name' => (string)$item->Description,
                    'price' => (float)$item->Value,
                    'code' => $code,
                    'group' => $attributes,
            ];
        }
        
        $this->data = $data;
        
        return $data;
    }
}