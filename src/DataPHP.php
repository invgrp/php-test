<?php
/**
 * Created by PhpStorm.
 * User: vgdomski
 * Date: 25.04.16
 * Time: 21:37
 */

namespace InnovationGroup\Data;


class DataPhp extends Data implements IData
{
    public function load()
    {
        $array = include $this->file;

        foreach ($array as $group => &$sections){
            foreach ($sections as $code => &$items){
                $items['code'] = $code;
                $items['group'] = $group;
                $items['price'] = $items['value'];
                unset($items['value']);
            }
        }

        $this->data = $array;

        return $array;
    }
}