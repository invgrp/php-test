<?php

/**
 * Created by PhpStorm.
 * User: vgdomski
 * Date: 27.04.16
 * Time: 6:54
 */

require_once '../../vendor/autoload.php';

require_once '../IData.php';
require_once '../Data.php';
require_once '../DataJSON.php';
require_once '../DataXML.php';
require_once '../DataPHP.php';

class DataTest extends PHPUnit_Framework_TestCase
{
    public function setUp(){ }
    public function tearDown(){ }

    public function testLoadJsonData()
    {
        $data_class = new \InnovationGroup\Data\DataJson('../../data/data.json');

        $this->assertTrue(!empty($data_class->get_data()));
    }

    public function testLoadPHPData()
    {
        $data_class = new \InnovationGroup\Data\DataXml('../../data/data.xml');

        $this->assertTrue(!empty($data_class->get_data()));
    }

    public function testLoadXMLData()
    {
        $data_class = new \InnovationGroup\Data\DataPhp('../../data/data.php');

        $this->assertTrue(!empty($data_class->get_data()));
    }
}