<?php
/**
 * Created by PhpStorm.
 * User: vgdomski
 * Date: 25.04.16
 * Time: 21:39
 */

namespace InnovationGroup\Data;


abstract class Data implements IData
{
    public $file;

    protected $data;

    protected $params;
    
    public function __construct($file)
    {
        $this->file = $file;
        $this->check_file();
    }

    public function load(){
        throw new \Exception("Data class can't load data. In: " . __FILE__, 503);
    }

    public function data(){
        throw new \Exception('No data provider!', 503);
    }

    public function get_data(){

        $data = [];
        foreach ($this->data as $section => $values) {
            foreach ($values as $code => $item) {
                $data[] = $item;
            }
        }

        return $data;
    }

    public function format($params){

        $this->params = $params;


        if(isset($params['filter'][0])) {
            $this->filter($params['filter'][0]);
        }

        if(isset($params['group'])) {
            $this->group($params['group']);
        }
        
        if(isset($params['sort_column'])&&isset($params['sort_direction'])) {
            $this->sort($params['sort_column'], $params['sort_direction']);
        }

        return true;
    }

    private function filter($filter){

        $f_column = $filter[0];
        $f_operator = $filter[1];
        $f_val = $filter[2];

        if(empty($f_column)){
            return;
        }

        $filter_data = [];

        foreach ($this->data as $section => $values){
            foreach ($values as $code => $item){
                switch ($f_operator){
                    case '>' : {
                        if(isset($item[$f_column]) && $item[$f_column] > $f_val){
                            $filter_data[$section][$code] = $item;
                        }
                        break;
                    }
                    case '<' : {
                        if(isset($item[$f_column]) && $item[$f_column] < $f_val){
                            $filter_data[$section][$code] = $item;
                        }
                        break;
                    }
                    case 'like':{
                        if((isset($item[$f_column]) && (strpos($item[$f_column], $f_val) !== false))){
                             $filter_data[$section][$code] = $item;
                        }
                        break;
                    }
                    default:{
                        throw new \Exception("Bad filter operator '{$f_operator}'!", 503);
                        break;
                    }
                }
            }
        }
        
        $this->data = $filter_data;

    }
    private function group($group){
        if($group != 'all') {
            $this->data = isset($this->data[$group]) ? [ $group => $this->data[$group]]  : [$group => []];
        }
    }

    private function sort($sort_column, $sort_direction){
        if($sort_column != 'unsorted'){
            foreach ($this->data as $section => &$values) {
                $sort_keys = [];
                foreach ($values as $key => $row)
                {
                    $sort_keys[$key] = $row[$sort_column];
                }
                array_multisort($sort_keys, ($sort_direction) == 'descending' ? SORT_DESC : SORT_ASC, $values);
            }
        }
    }

    protected function check_file(){
        if(file_exists($this->file)) {
            return true;
        }
        else {
            throw new \Exception("File can't be load, bad path: {$this->file} !", 404);
        }
    }
}