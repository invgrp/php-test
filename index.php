<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

</head>
<body>

    <form action="data.php" method="post">
        Data source:
        <select name="data_source" title="data source">
            <option>php</option>
            <option>json</option>
            <option>xml</option>
        </select> <br />

        Group: <input name="group" title="select group"/> <br />
        Order column: <input name="order_column" title="order column"/>
        Order direction:
        <select name="order_direction" title="order direction">
            <option>ascending</option>
            <option>descending</option>
        </select>
        <br />
        Filter:
        <input name="filter_0" title="filter column"/>
        <input name="filter_1" title="filter operator"/>
        <input name="filter_2" title="filter value"/>

        <br />

        <button type="submit">Send</button>

        <div id="data">

        </div>

    </form>

<script>
    $(document).ready(function() {

        $('form').submit(function(event) {
            
            /* var formData = {
                'data_source'              : $('select[name=data_source]').val(),
                'group'             : $('input[name=group]').val(),
                'order_column'    : $('input[name=order_column]').val(),
                'order_direction'    : $('select[name=order_direction]').val(),
                'filter'    : [
                    $('input[name=filter_0]').val(),
                    $('input[name=filter_1]').val(),
                    $('input[name=filter_2]').val(),
                ],
            }; */

             var formData =
             'data_source='  + $('select[name=data_source]').val() +
             '&group='             +  $('input[name=group]').val() +
             '&sort_column='    +  $('input[name=order_column]').val() +
             '&sort_direction='   + $('select[name=order_direction]').val() +
             '&filter[0][0]='  +  $('input[name=filter_0]').val() +
             '&filter[0][1]='  +  $('input[name=filter_1]').val() +
             '&filter[0][2]='  +  $('input[name=filter_2]').val();

            alert(JSON.stringify(formData));

            $.ajax({
                    type        : 'POST',
                    url         : 'data.php',
                    data        : formData,
                    dataType    : 'json',
                    encode          : true
                })
                .done(function(data) {
                    var data_item = document.getElementById("data");
                    var table = '<table>';

                    table += "<tr>" +
                        "<th>Group</th>" +
                        "<th>Code</th>" +
                        "<th>Name</th>" +
                        "<th>Price</th>" +
                        "</tr>";

                    for(var i = 0; i < data.length; i++){
                        table += "<tr>" +
                            "<td>" + data[i]["group"] + "</td>" +
                            "<td>" + data[i]["code"] + "</td>" +
                            "<td>" + data[i]["name"] + "</td>" +
                            "<td>" + data[i]["price"] + "</td>" +
                            "</tr>";
                    }

                    table += "</table>";

                    data_item.innerHTML = table;

                });
            event.preventDefault();
        });

    });
</script>
<style>
    table tr th,td{
        padding: 15px;
    }
</style>
</body>
</html>