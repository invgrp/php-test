<?php

/**
 * Created by PhpStorm.
 * User: vgdomski
 * Date: 24.04.16
 * Time: 21:56
 */

/** 
 * TODO: autoloader composer 
 */
require_once __DIR__ . '/src/IData.php';
require_once __DIR__ . '/src/Data.php';
require_once __DIR__ . '/src/DataJSON.php';
require_once __DIR__ . '/src/DataPHP.php';
require_once __DIR__ . '/src/DataXML.php';
require_once __DIR__ . '/src/DataProvider.php';
require_once __DIR__ . '/src/Params/Validator.php';


$params = $_POST;
$params = \InnovationGroup\Params\Validator::validate($params);

if(!$params[0]){
    echo $params[1];
}
else {

    $data = \InnovationGroup\Data\DataProvider::get($params['data_source']);
    $data = new $data(__DIR__ . '/data/data.' . $params['data_source']);
    $array = $data->load();
    
    $data->format($params);
    echo json_encode($data->get_data());
}